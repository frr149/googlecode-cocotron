#import <QuartzCore/CARenderer.h>

@interface CABackingRenderer : CARenderer {
}

-(void)renderWithSurface:(O2Surface *)surface;

@end
